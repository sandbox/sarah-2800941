<?php

/**
 * API calls to Yumpu.
 */

class Yumpu {

  protected $token;

  public function __construct($token)
  {
    $this->token = $token;
  }


  public function getEmbeds()
  {
    $url = 'http://api.yumpu.com/2.0/embeds.json?offset=0&limit=100&sort=desc&return_fields=id,code,document_id';
    return $this->executeRequest($url);
  }


  public function executeRequest($url)
  {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0');
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'X-ACCESS-TOKEN: ' . $this->token,
    ));
    ob_start(); curl_exec($ch);
    $raw = ob_get_contents();
    ob_end_clean();
    curl_close($ch);

    $json = json_decode($raw);
    return $json;
  }

}